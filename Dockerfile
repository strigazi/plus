FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:20191002

# Start from empty cache
RUN yum clean all

# Base packages
RUN yum install -y \
    bc \
    man-pages \
    openafs-krb5 \
    passwd \
    python3-pip \
    yum-plugin-priorities

# Condor packages
RUN yum install -y \
    cernbatchsubmit \
    condor \
    condor-classads \
    condor-external-libs \
    condor-procd \
    condor-python \
    condor-std-universe \
    ngbauth-submit

# OpenStack packages
RUN yum install -y \
	python-barbicanclient \
	python-decorator \
	python-heatclient \
	python-ironic-inspector-client \
	python-keystoneclient-x509 \
	python-openstackclient \
	python-swiftclient \
	python2-cryptography \
	python2-ironicclient \
	python2-magnumclient \
	python2-manilaclient \
	python2-mistralclient

# Cleanup caches to reduce image size
RUN yum clean all

# Jupyter environment setup
RUN pip3 install --no-cache --upgrade pip && \
    pip3 install --no-cache jupyterhub==1.0.0 && \
    pip3 install --no-cache jupyterlab==1.1.4 && \
    pip3 install --no-cache notebook==6.0.1

ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}
ENV SHELL bash

RUN adduser --uid ${NB_UID} ${NB_USER} && passwd -d ${NB_USER}

WORKDIR ${HOME}
USER ${USER}

# Base configuration
ADD etc /etc
ADD jupyterlab-workspace.json start /

# User level data
ADD images ${HOME}/images
ADD Welcome.md ${HOME}/

RUN ln -s /eos ${HOME}/eos

ENTRYPOINT /start
