<img src="images/cern-logo.png" width="200px" align="right">

## Plus Environment

Welcome to your Plus web based environment.

**NOTE**: Create your kerberos token before navigating the left pane (this will go away soon)
```bash
kinit YOURUSER
```

This environment should feel familiar to lxplus, only web based.

### Customize

Each environment runs in a separate container and can be tuned to fit your needs. [Check this repo](https://gitlab.cern.ch/binder/repos/plus) and feel free to fork and send back a merge request if appropriate.
feel free to fork and send back a merge request if appropriate.
